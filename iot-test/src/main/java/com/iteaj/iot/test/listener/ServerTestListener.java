package com.iteaj.iot.test.listener;

import com.iteaj.iot.server.ServerEventListener;
import com.iteaj.iot.server.SocketServerComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnExpression("${iot.test.start-listener:false}")
public class ServerTestListener implements ServerEventListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void online(String source, SocketServerComponent component) {
        logger.info("服务端同步事件监听测试({}) 上线 - 客户端编号：{} - 通过", component.getName(), source);
    }

    @Override
    public void offline(String source, SocketServerComponent component) {
        logger.info("服务端同步事件监听测试({}) 掉线 - 客户端编号：{} - 通过", component.getName(), source);
    }
}
