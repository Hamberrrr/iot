package com.iteaj.iot.event;

/**
 * 离线原因
 */
public enum OfflineReason {

    close("正常关闭"),
    timeout("读写超时"),

    ;

    private String reason;

    OfflineReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
