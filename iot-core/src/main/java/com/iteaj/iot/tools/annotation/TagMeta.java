package com.iteaj.iot.tools.annotation;

import com.iteaj.iot.tools.db.DBMeta;
import com.iteaj.iot.tools.db.FieldMeta;

import java.util.List;

public interface TagMeta extends DBMeta {

    /**
     * tag字段元列表
     * @see IotTag
     * @return
     */
    List<? extends FieldMeta> getTagMetas();
}
