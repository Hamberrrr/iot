package com.iteaj.iot;

public interface DeviceManager<T> {

    /**
     * 有报文交互过的客户端数量
     * @return
     */
    int useSize();

    /**
     * 通过设备编号获取对应的设备信息
     * @param equipCode 设备编号
     * @return
     */
    T find(String equipCode);

    /**
     * 移除指定设备
     * @param equipCode
     * @return
     */
    T remove(String equipCode);

    /**
     * 关闭指定设备
     * @param equipCode
     */
    boolean close(String equipCode);

    /**
     * 指定设备是否关闭
     * @param equipCode
     * @return
     */
    boolean isClose(String equipCode);

    /**
     * 新增对应的设备
     * @param equipCode 设备编号
     * @param device 对应的连接
     * @return
     */
    T add(String equipCode, T device);
}
